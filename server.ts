import { startBot, getChannel } from 'https://deno.land/x/discordeno/mod.ts'

import 'https://deno.land/x/dotenv/load.ts'
import { Mastodon } from './mastodon.ts'

const MASTODON_TOKEN = Deno.env.get('MASTODON_TOKEN') || ''
const GUILD_ID = Deno.env.get('DISCORD_GUILD_ID') || ''
const MASTODON_URL = Deno.env.get('MASTODON_URL') || ''
const DISCORD_TOKEN = Deno.env.get('DISCORD_TOKEN') || ''

console.info(DISCORD_TOKEN)

const mastodon = new Mastodon(MASTODON_TOKEN, MASTODON_URL)

startBot({
  token: DISCORD_TOKEN,
  intents: ['GUILD_VOICE_STATES', 'GUILDS'],
  eventHandlers: {
    async voiceChannelJoin(member, channelID) {
      const channel = await getChannel(channelID)
      if (channel.connectedMembers!.size !== 1) {
        console.debug('He(She) is not the first member.')
        return
      }
      const userNickname = member.name(channel.guildID)
      const channelName = channel.name!
      const content = `【Discord通知】\n${channelName}に${userNickname}が参加しました．`
      console.info(content)
      await mastodon.toot(content)
    },
    ready () {
      console.info('Ready.')
    }
  }
})
.then()
.catch((e) => console.error(e))
