export class Mastodon {
  token = ''
  baseUrl = ''

  constructor (token: string, baseUrl: string) {
    this.token = token
    this.baseUrl = baseUrl
  }

  async toot (status: string) {
    const res = await fetch(`${this.baseUrl}/api/v1/statuses`, {
      method: 'POST',
      headers: {
        authorization: `Bearer ${this.token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'status': status
      })
    })
    if (res.status !== 200) {
      console.error(await res.json())
      console.error(`${res.status} error happened.`)
    }
    const json = await res.json()
  }
}