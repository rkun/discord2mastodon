# discord2mastodon

## DiscordでVCに参加したことをMastodonにしゃべるBot
- ボイスチャットに対し，誰かが初めて参加した際にMastodonでしゃべる

## RUN
1. `$ cp .env.default .env`
1. `.env`を整備
1. `$ deno run --allow-read --allow-env --allow-net server.ts`
